import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { bodyPartsDelete, bodyPartsGetAll } from "src/services/services";
import { AddBodyParts } from "src/components/bodyParts/AddBodyParts";
import { BodyPartsListResults } from "src/components/bodyParts/bodyParts-list-results";
import { BodyPartsListToolbar } from "src/components/bodyParts/bodyParts-list-toolbar";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const BodyParts = () => {
  const [addBodyPartsStatus, setAddBodyPartsStatus] = useState(false);
  const [editBodyPartsStatus, setEditBodyPartsStatus] = useState(false);
  const [listOfBodyParts, setListOfBodyParts] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [bodyPartsIdForDeletion, setBodyPartsIdForDeletion] = useState("");
  const [bodyPartsForEdit, setBodyPartsForEdit] = useState("");

  useEffect(() => {
    if (!addBodyPartsStatus) {
      bodyPartsGetAll().then((res) => {
        //GetAll BodyParts api call which returns all the BodyParts.
        if (res?.length > 0) setListOfBodyParts(res);
      });
    }
  }, [addBodyPartsStatus, confirmOpen, editBodyPartsStatus]);

  const handleBodyPartsDelete = (bodyPartsId) => {
    setBodyPartsIdForDeletion(bodyPartsId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    bodyPartsDelete(bodyPartsIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleBodyPartsEdit = (bodyParts) => {
    setBodyPartsForEdit(bodyParts);
    setEditBodyPartsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Body Parts| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addBodyPartsStatus || editBodyPartsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <BodyPartsListToolbar
                setAddBodyPartsStatus={setAddBodyPartsStatus}
                addBodyPartsStatus={addBodyPartsStatus}
                editBodyPartsStatus={editBodyPartsStatus}
              />
              <AddBodyParts
                setAddBodyPartsStatus={setAddBodyPartsStatus}
                setEditBodyPartsStatus={setEditBodyPartsStatus}
                editBodyPartsStatus={editBodyPartsStatus}
                bodyPartsForEdit={bodyPartsForEdit}
                setBodyPartsForEdit={setBodyPartsForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <BodyPartsListToolbar
              setAddBodyPartsStatus={setAddBodyPartsStatus}
              addBodyPartsStatus={addBodyPartsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <BodyPartsListResults
                listOfBodyParts={listOfBodyParts}
                handleBodyPartsDelete={handleBodyPartsDelete}
                handleBodyPartsEdit={handleBodyPartsEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Body Part?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
BodyParts.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default BodyParts;
