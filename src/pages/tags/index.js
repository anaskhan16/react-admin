import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { tagsDelete, tagsGetAll } from "src/services/services";
import { TagsListToolbar } from "src/components/tags/tags-list-toolbar";
import { TagsListResults } from "src/components/tags/tags-list-results";
import { AddTags } from "src/components/tags/AddTags";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const Tags = () => {
  const [addTagsStatus, setAddTagsStatus] = useState(false);
  const [editTagsStatus, setEditTagsStatus] = useState(false);
  const [listOfTags, setListOfTags] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [tagIdForDeletion, setTagIdForDeletion] = useState("");
  const [tagForEdit, setTagForEdit] = useState("");

  useEffect(() => {
    if (!addTagsStatus) {
      tagsGetAll().then((res) => {
        //GetAll tags api call which returns all the tags.
        if (res?.length > 0) setListOfTags(res);
      });
    }
  }, [addTagsStatus, confirmOpen, editTagsStatus]);

  const handleTagDelete = (tagId) => {
    setTagIdForDeletion(tagId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    tagsDelete(tagIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleTagEdit = (tag) => {
    setTagForEdit(tag);
    setEditTagsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Tags | OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addTagsStatus || editTagsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <TagsListToolbar
                setAddTagsStatus={setAddTagsStatus}
                addTagsStatus={addTagsStatus}
                editTagsStatus={editTagsStatus}
              />
              <AddTags
                setAddTagsStatus={setAddTagsStatus}
                setEditTagsStatus={setEditTagsStatus}
                editTagsStatus={editTagsStatus}
                tagForEdit={tagForEdit}
                setTagForEdit={setTagForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <TagsListToolbar
              setAddTagsStatus={setAddTagsStatus}
              addTagsStatus={addTagsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <TagsListResults
                listOfTags={listOfTags}
                handleTagDelete={handleTagDelete}
                handleTagEdit={handleTagEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Tag?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
Tags.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Tags;
