import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { partnershipsDelete, partnershipsGetAll } from "src/services/services";
import { ParnershipsListToolbar } from "src/components/partnerships/partnerships-list-toolbar";
import { PartnershipsListResults } from "src/components/partnerships/partnerships-list-results";
import { AddPartners } from "src/components/partnerships/AddPartner";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const Partnerships = () => {
  const [addPartnershipsStatus, setAddParnershipsStatus] = useState(false);
  const [editPartnershipsStatus, setEditPartnershipsStatus] = useState(false);
  const [listOfPartnerships, setListOfPartnerships] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [partnershipsIdForDeletion, setPartnershipsIdForDeletion] = useState(
    ""
  );
  const [partnershipsForEdit, setPartnershipsForEdit] = useState("");

  useEffect(() => {
    if (!addPartnershipsStatus) {
      partnershipsGetAll().then((res) => {
        //GetAll Partnerships api call which returns all the Partnerships.
        if (res?.length > 0) setListOfPartnerships(res);
      });
    }
  }, [addPartnershipsStatus, confirmOpen, editPartnershipsStatus]);

  const handlePartnershipsDelete = (partnershipsId) => {
    setPartnershipsIdForDeletion(partnershipsId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    partnershipsDelete(partnershipsIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handlePartnershipsEdit = (partnerships) => {
    setPartnershipsForEdit(partnerships);
    setEditPartnershipsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Parnerships| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addPartnershipsStatus || editPartnershipsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <ParnershipsListToolbar
                setAddParnershipsStatus={setAddParnershipsStatus}
                addPartnershipsStatus={addPartnershipsStatus}
                editPartnershipsStatus={editPartnershipsStatus}
              />
              <AddPartners
                setAddParnershipsStatus={setAddParnershipsStatus}
                setEditPartnershipsStatus={setEditPartnershipsStatus}
                editPartnershipsStatus={editPartnershipsStatus}
                partnershipsForEdit={partnershipsForEdit}
                setPartnershipsForEdit={setPartnershipsForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <ParnershipsListToolbar
              setAddParnershipsStatus={setAddParnershipsStatus}
              addPartnershipsStatus={addPartnershipsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <PartnershipsListResults
                listOfPartnerships={listOfPartnerships}
                handlePartnershipsDelete={handlePartnershipsDelete}
                handlePartnershipsEdit={handlePartnershipsEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Partnerships?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
Partnerships.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Partnerships;
