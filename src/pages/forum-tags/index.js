import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { forumTagsDelete, forumTagsGetAll } from "src/services/services";
import { ForumTagsListToolbar } from "src/components/forum-tags/forum-tags-list-toolbar";
import { ForumTagsListResults } from "src/components/forum-tags/forum-tags-list-result";
import { AddForumTags } from "src/components/forum-tags/AddForum-Tags";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const Forums = () => {
  const [addForumTagsStatus, setAddForumTagsStatus] = useState(false);
  const [editForumTagsStatus, setEditForumTagsStatus] = useState(false);
  const [listOfForumTags, setListOfForumTags] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [forumTagsIdForDeletion, setForumTagsIdForDeletion] = useState("");
  const [forumTagsForEdit, setForumTagsForEdit] = useState("");

  useEffect(() => {
    if (!addForumTagsStatus) {
      forumTagsGetAll().then((res) => {
        //GetAll tags api call which returns all the tags.
        if (res?.length > 0) setListOfForumTags(res);
      });
    }
  }, [addForumTagsStatus, confirmOpen, editForumTagsStatus]);

  const handleForumTagsDelete = (forumTagId) => {
    setForumTagsIdForDeletion(forumTagId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    forumTagsDelete(forumTagsIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleForumTagsEdit = (forumTag) => {
    setForumTagsForEdit(forumTag);
    setEditForumTagsStatus(true);
  };

  return (
    <>
      <Head>
        <title> Forum Tags | OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addForumTagsStatus || editForumTagsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <ForumTagsListToolbar
                setAddForumTagsStatus={setAddForumTagsStatus}
                addForumTagsStatus={addForumTagsStatus}
                editForumTagsStatus={editForumTagsStatus}
              />
              <AddForumTags
                setAddForumTagsStatus={setAddForumTagsStatus}
                setEditForumTagsStatus={setEditForumTagsStatus}
                editForumTagsStatus={editForumTagsStatus}
                forumTagsForEdit={forumTagsForEdit}
                setForumTagsForEdit={setForumTagsForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <ForumTagsListToolbar
              setAddForumTagsStatus={setAddForumTagsStatus}
              addForumTagsStatus={addForumTagsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <ForumTagsListResults
                listOfForumTags={listOfForumTags}
                handleForumTagsDelete={handleForumTagsDelete}
                handleForumTagsEdit={handleForumTagsEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Forum Tag?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
Forums.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Forums;
