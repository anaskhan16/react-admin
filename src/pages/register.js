import Head from "next/head";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Box,
  Button,
  Checkbox,
  Container,
  FormHelperText,
  Link,
  TextField,
  Typography,
  Grid,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { signUpUser, userDataUpdate } from "src/services/services";
import { useEffect, useState } from "react";

const Register = () => {
  const router = useRouter();
  const [Editting, setEditting] = useState(false);

  useEffect(() => {
    if (router?.query) {
      setEditting(true);
    }
  }, [router?.query]);
  const returnInitialValues = () => {
    if (router?.query) {
      const picked = (({
        email,
        firstName,
        lastName,
        password,
        age,
        country,
        state,
        address,
        phoneNo,
        zip,
        city,
      }) => ({
        email,
        firstName,
        lastName,
        password,
        age,
        country,
        state,
        address,
        phoneNo,
        zip,
        city,
      }))(router?.query);
      return picked;
    }

    return {
      email: "",
      firstName: "",
      lastName: "",
      password: "",
      age: "",
      country: "",
      state: "",
      address: "",
      phoneNo: "",
      zip: "",
      city: "",
    };
  };
  const formik = useFormik({
    initialValues: returnInitialValues(),
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Must be a valid email")
        .max(255)
        .required("Email is required"),
      firstName: Yup.string().max(255).required("First name is required"),
      lastName: Yup.string().max(255).required("Last name is required"),
      password: Yup.string().max(255).required("Password is required"),
      age: Yup.number()
        .typeError("Enter correct age")
        .min(6, "Age should not be 0")
        .max(140, "Invalid input")
        .required("Age is required"),
      country: Yup.string().max(255).required("Country is required"),
      state: Yup.string().max(255).required("State is required"),
      address: Yup.string().max(255).required("Address is required"),
      phoneNo: Yup.string().required("Phone number is required"),
      zip: Yup.number()
        .typeError("Enter valid zip")
        .required("Zip is required"),
      city: Yup.string().max(255).required("City is required"),
    }),
    onSubmit: () => {
      checkFormData();
    },
  });

  const checkFormData = () => {
    if (Editting) {
      userDataUpdate(router?.query?._id, formik.values)
        .then((res) => {
          if (res?.responseStatus?.success) {
            setEditting(false);
            router.push("/");
          }
        })
        .catch((error) => {
          console.log("error", error);
        });
    } else
      signUpUser(formik.values)
        .then((res) => {
          if (res?.responseStatus?.success) {
            router.push("/login");
          }
        })
        .catch((error) => {
          console.log("error", error);
        });
  };
  return (
    <>
      <Head>
        <title>Register | OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          alignItems: "center",
          display: "flex",
          flexGrow: 1,
          minHeight: "100%",
        }}
      >
        <Container maxWidth="md">
          <form onSubmit={formik.handleSubmit}>
            <Box sx={{ my: 3 }}>
              <Typography color="textPrimary" variant="h4">
                {Editting ? "Edit User Details" : "Create a new account"}
              </Typography>
              {!Editting && (
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Use your email to create a new account
                </Typography>
              )}
            </Box>
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.firstName && formik.errors.firstName
                  )}
                  fullWidth
                  helperText={
                    formik.touched.firstName && formik.errors.firstName
                  }
                  label="First Name"
                  margin="normal"
                  name="firstName"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.firstName}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.lastName && formik.errors.lastName
                  )}
                  fullWidth
                  helperText={formik.touched.lastName && formik.errors.lastName}
                  label="Last Name"
                  margin="normal"
                  name="lastName"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.lastName}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  error={Boolean(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Email Address"
                  margin="normal"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                  variant="outlined"
                />
              </Grid>{" "}
              <Grid item md={6} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.password && formik.errors.password
                  )}
                  fullWidth
                  helperText={formik.touched.password && formik.errors.password}
                  label="Password"
                  margin="normal"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <TextField
                  error={Boolean(formik.touched.age && formik.errors.age)}
                  fullWidth
                  helperText={formik.touched.age && formik.errors.age}
                  label="Age"
                  margin="normal"
                  name="age"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.age}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <TextField
                  error={Boolean(formik.touched.zip && formik.errors.zip)}
                  fullWidth
                  helperText={formik.touched.zip && formik.errors.zip}
                  label="Zip"
                  margin="normal"
                  name="zip"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.zip}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={5} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.country && formik.errors.country
                  )}
                  fullWidth
                  helperText={formik.touched.country && formik.errors.country}
                  label="Country"
                  margin="normal"
                  name="country"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.country}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <TextField
                  error={Boolean(formik.touched.state && formik.errors.state)}
                  fullWidth
                  helperText={formik.touched.state && formik.errors.state}
                  label="State"
                  margin="normal"
                  name="state"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.state}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <TextField
                  error={Boolean(formik.touched.city && formik.errors.city)}
                  fullWidth
                  helperText={formik.touched.city && formik.errors.city}
                  label="City"
                  margin="normal"
                  name="city"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.city}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.address && formik.errors.address
                  )}
                  fullWidth
                  helperText={formik.touched.address && formik.errors.address}
                  label="Address"
                  margin="normal"
                  name="address"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.address}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <TextField
                  error={Boolean(
                    formik.touched.phoneNo && formik.errors.phoneNo
                  )}
                  fullWidth
                  helperText={formik.touched.phoneNo && formik.errors.phoneNo}
                  label="Phone Number"
                  margin="normal"
                  name="phoneNo"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.phoneNo}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={8} xs={12}>
                <>
                  <Box sx={{ py: 2 }}>
                    <Button
                      color="primary"
                      // disabled={formik.isSubmitting}
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                    >
                      {!Editting ? "Sign Up Now" : "Save Changes"}
                    </Button>
                  </Box>
                  {!Editting && (
                    <Typography color="textSecondary" variant="body2">
                      Have an account?{" "}
                      <NextLink href="/login" passHref>
                        <Link variant="subtitle2" underline="hover">
                          Sign In
                        </Link>
                      </NextLink>
                    </Typography>
                  )}
                </>
              </Grid>
            </Grid>
          </form>
        </Container>
      </Box>
    </>
  );
};

export default Register;
