import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { exerciseTypeDelete, exerciseTypeGetAll } from "src/services/services";
import { AddExerciseTypes } from "src/components/exerciseTypes/AddExerciseTypes";
import { ExerciseTypesListResults } from "src/components/exerciseTypes/exerciseTypes-list-results";
import { ExerciseTypesListToolbar } from "src/components/exerciseTypes/exerciseTypes-list-toolbar";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const ExerciseTypes = () => {
  const [addExerciseTypesStatus, setAddExerciseTypesStatus] = useState(false);
  const [editExerciseTypesStatus, setEditExerciseTypesStatus] = useState(false);
  const [listOfExerciseTypes, setListOfExerciseTypes] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [exerciseTypeIdForDeletion, setExerciseTypeIdForDeletion] = useState(
    ""
  );
  const [exerciseTypeForEdit, setExerciseTypeForEdit] = useState("");

  useEffect(() => {
    if (!addExerciseTypesStatus) {
      exerciseTypeGetAll().then((res) => {
        //GetAll ExerciseTypes api call which returns all the ExerciseTypes.
        if (res?.length > 0) setListOfExerciseTypes(res);
      });
    }
  }, [addExerciseTypesStatus, confirmOpen, editExerciseTypesStatus]);

  const handleExerciseTypeDelete = (exerciseTypeId) => {
    setExerciseTypeIdForDeletion(exerciseTypeId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    exerciseTypeDelete(exerciseTypeIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleExerciseTypeEdit = (exerciseType) => {
    setExerciseTypeForEdit(exerciseType);
    setEditExerciseTypesStatus(true);
  };

  return (
    <>
      <Head>
        <title>ExerciseType| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addExerciseTypesStatus || editExerciseTypesStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <ExerciseTypesListToolbar
                setAddExerciseTypesStatus={setAddExerciseTypesStatus}
                addExerciseTypesStatus={addExerciseTypesStatus}
                editExerciseTypesStatus={editExerciseTypesStatus}
              />
              <AddExerciseTypes
                setAddExerciseTypesStatus={setAddExerciseTypesStatus}
                setEditExerciseTypesStatus={setEditExerciseTypesStatus}
                editExerciseTypesStatus={editExerciseTypesStatus}
                exerciseTypeForEdit={exerciseTypeForEdit}
                setExerciseTypeForEdit={setExerciseTypeForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <ExerciseTypesListToolbar
              setAddExerciseTypesStatus={setAddExerciseTypesStatus}
              addExerciseTypesStatus={addExerciseTypesStatus}
            />
            <Box sx={{ mt: 3 }}>
              <ExerciseTypesListResults
                listOfExerciseTypes={listOfExerciseTypes}
                handleExerciseTypeDelete={handleExerciseTypeDelete}
                handleExerciseTypeEdit={handleExerciseTypeEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Exercise Type?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
ExerciseTypes.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default ExerciseTypes;
