import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import {
  exclusionCriteriaDelete,
  exclusionCriteriaGetAll,
} from "src/services/services";
import { ExclusionCriteriaListToolbar } from "src/components/exclusion-criteria/exclusion-criteria-list-toolbar";
import { ExclusionCriteriaListResults } from "src/components/exclusion-criteria/exclusion-criteria-list-result";
import { AddExclusionCriteria } from "src/components/exclusion-criteria/AddExclusion-Criteria";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const ExclusionCriteria = () => {
  const [addExclusionCriteriaStatus, setAddExclusionCriteriaStatus] = useState(
    false
  );
  const [
    editExclusionCriteriaStatus,
    setEditExclusionCriteriaStatus,
  ] = useState(false);
  const [listOfExclusionCriteria, setListOfExclusionCriteria] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [exclusionIdForDeletion, setExclusionIdForDeletion] = useState("");
  const [exclusionCriteriaForEdit, setExclusionCriteriaForEdit] = useState("");

  useEffect(() => {
    if (!addExclusionCriteriaStatus) {
      exclusionCriteriaGetAll().then((res) => {
        //GetAll exclusionCriteria api call which returns all the exclusionCriteria.
        if (res?.length > 0) setListOfExclusionCriteria(res);
      });
    }
  }, [addExclusionCriteriaStatus, confirmOpen, editExclusionCriteriaStatus]);

  const handleExclusionCriteriaDelete = (exclusionCriteriaId) => {
    setExclusionIdForDeletion(exclusionCriteriaId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    exclusionCriteriaDelete(exclusionIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleExclusionCriteriaEdit = (exclusion) => {
    setExclusionCriteriaForEdit(exclusion);
    setEditExclusionCriteriaStatus(true);
  };

  return (
    <>
      <Head>
        <title>Exclusion Criteria| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addExclusionCriteriaStatus || editExclusionCriteriaStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <ExclusionCriteriaListToolbar
                setAddExclusionCriteriaStatus={setAddExclusionCriteriaStatus}
                addExclusionCriteriaStatus={addExclusionCriteriaStatus}
                editExclusionsCriteriaStatus={editExclusionCriteriaStatus}
              />
              <AddExclusionCriteria
                setAddExclusionCriteriaStatus={setAddExclusionCriteriaStatus}
                setEditExclusionCriteriaStatus={setEditExclusionCriteriaStatus}
                editExclusionCriteriaStatus={editExclusionCriteriaStatus}
                exclusionCriteriaForEdit={exclusionCriteriaForEdit}
                setExclusionCriteriaForEdit={setExclusionCriteriaForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <ExclusionCriteriaListToolbar
              setAddExclusionCriteriaStatus={setAddExclusionCriteriaStatus}
              addExclusionCriteriaStatus={addExclusionCriteriaStatus}
            />
            <Box sx={{ mt: 3 }}>
              <ExclusionCriteriaListResults
                listOfExclusionCriteria={listOfExclusionCriteria}
                handleExclusionCriteriaDelete={handleExclusionCriteriaDelete}
                handleExclusionCriteriaEdit={handleExclusionCriteriaEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Exclusion?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
ExclusionCriteria.getLayout = (page) => (
  <DashboardLayout>{page}</DashboardLayout>
);

export default ExclusionCriteria;
