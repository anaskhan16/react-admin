import Head from "next/head";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Box,
  Button,
  Container,
  Link,
  TextField,
  Typography,
  Checkbox,
  FormGroup,
  FormControlLabel,
} from "@mui/material";
import { loginAdmin } from "src/services/services";
import { useState } from "react";

const Login = () => {
  const router = useRouter();
  const [invalidCredError, setInvalidCredError] = useState(false);

  const redirectToDashboard = () => {
    if (sessionStorage?.userLoggedIn) {
      router.push("/");
    }
  };

  const returnInitialValues = () => {
    const userSession = localStorage?.userLoggedIn
      ? JSON.parse(localStorage?.userLoggedIn)
      : false;
    if (userSession) {
      return {
        username: userSession?.username,
        password: userSession?.password,
        remember: true,
      };
    }

    return {
      username: "",
      password: "",
      remember: true,
    };
  };

  const formik = useFormik({
    initialValues: returnInitialValues(),
    validationSchema: Yup.object({
      username: Yup.string().max(255).required("Username is required"),
      password: Yup.string().max(255).required("Password is required"),
    }),
    onSubmit: () => {
      checkCredentials();
    },
  });

  const checkCredentials = () => {
    setInvalidCredError(false);

    loginAdmin({
      username: formik.values.username,
      password: formik.values.password,
    })
      .then((res) => {
        if (!res?.log) {
          setInvalidCredError(true);
        } else {
          sessionStorage.setItem("userLoggedIn", JSON.stringify(res?.tokens));
          if (formik.values.remember) {
            localStorage.setItem("userLoggedIn", JSON.stringify(res?.log));
          } else {
            localStorage.removeItem("userLoggedIn");
          }
          redirectToDashboard();
        }
      })
      .catch((error) => {
        console.log("error", error);
        setInvalidCredError(true);
      });
  };
  return (
    <>
      <Head>
        <title>Login | Optimi Health</title>
      </Head>
      <Box
        component="main"
        sx={{
          alignItems: "center",
          display: "flex",
          flexGrow: 1,
          minHeight: "100%",
        }}
      >
        <Container maxWidth="sm">
          <form onSubmit={formik.handleSubmit}>
            <Box sx={{ my: 3 }}>
              <Typography color="textPrimary" variant="h4">
                Sign in
              </Typography>
              {invalidCredError ? (
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Invalid credentials entered
                </Typography>
              ) : (
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Sign in on the internal platform
                </Typography>
              )}
            </Box>

            <TextField
              error={Boolean(formik.touched.username && formik.errors.username)}
              fullWidth
              helperText={formik.touched.username && formik.errors.username}
              label="Username"
              margin="normal"
              name="username"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.username}
              variant="outlined"
            />
            <TextField
              error={Boolean(formik.touched.password && formik.errors.password)}
              fullWidth
              helperText={formik.touched.password && formik.errors.password}
              label="Password"
              margin="normal"
              name="password"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="password"
              value={formik.values.password}
              variant="outlined"
            />
            <Box sx={{ py: 2 }}>
              <Button
                color="primary"
                disabled={formik.isSubmitting && !invalidCredError}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Sign In Now
              </Button>
            </Box>

            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                ml: -1,
                alignContent: "space-between",
                justifyContent: "space-between",
              }}
            >
              <>
                {" "}
                <Typography color="textSecondary" variant="body2">
                  Don&apos;t have an account?{" "}
                  <NextLink href="/register">
                    <Link
                      to="/register"
                      variant="subtitle2"
                      underline="hover"
                      sx={{
                        cursor: "pointer",
                      }}
                    >
                      Sign Up
                    </Link>
                  </NextLink>
                </Typography>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={formik.values.remember}
                        name="remember"
                        onChange={formik.handleChange}
                      />
                    }
                    label="Remember Me"
                  />
                </FormGroup>
              </>
            </Box>
          </form>
        </Container>
      </Box>
    </>
  );
};

export default Login;
