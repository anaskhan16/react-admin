import Head from "next/head";
import { Box, Container } from "@mui/material";
import { PostsListResults } from "src/components/posts/posts-list-results";
import { DashboardLayout } from "../../components/dashboard-layout";
import { customers } from "../../__mocks__/customers";
import { PostsListToolbar } from "src/components/posts/posts-list-toolbar";
import { useEffect, useState } from "react";
import { AddPosts } from "src/components/posts/AddPosts";
import { postsDelete, postsGetAll, tagsGetAll } from "src/services/services";
import ConfirmDialog from "src/components/common/confirm-dialog";

const Posts = () => {
  const [addPostsStatus, setAddPostsStatus] = useState(false);
  const [editPostsStatus, setEditPostsStatus] = useState(false);
  const [listOfPosts, setListOfPosts] = useState([]);
  const [listOfTags, setListOfTags] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [totalResults, setTotalResults] = useState(0);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [postIdForDeletion, setPostIdForDeletion] = useState("");
  const [postForEdit, setPostForEdit] = useState({});

  useEffect(() => {
    tagsGetAll().then((res) => {
      //GetAll tags api call which returns all the tags.
      if (res?.length > 0) setListOfTags(res);
    });

    if (!addPostsStatus) {
      postsGetAll(page + 1, limit).then((res) => {
        //GetAll posts api call which returns all the posts.
        if (res?.results?.length > 0) {
          setListOfPosts(res?.results);
          setTotalResults(res?.totalResults);
        }
      });
    }
  }, [addPostsStatus, page, limit, confirmOpen]);

  const handlePostDelete = (postId) => {
    setPostIdForDeletion(postId);
    setConfirmOpen(true);
  };

  const handleConfirmDeletion = () => {
    postsDelete(postIdForDeletion).then((res) => {
      if (res?._id) setConfirmOpen(false);
    });
  };

  const handlePostEdit = (tag) => {
    setPostForEdit(tag);
    setEditPostsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Posts | OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addPostsStatus || editPostsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <PostsListToolbar
                setAddPostsStatus={setAddPostsStatus}
                addPostsStatus={addPostsStatus}
                editPostsStatus={editPostsStatus}
              />
              <AddPosts
                setAddPostsStatus={setAddPostsStatus}
                listOfTags={listOfTags}
                setEditPostsStatus={setEditPostsStatus}
                editPostsStatus={editPostsStatus}
                postForEdit={postForEdit}
                setPostForEdit={setPostForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <PostsListToolbar setAddPostsStatus={setAddPostsStatus} />
            <Box sx={{ mt: 3 }}>
              <PostsListResults
                customers={customers}
                listOfPosts={listOfPosts}
                listOfTags={listOfTags}
                limit={limit}
                setLimit={setLimit}
                setPage={setPage}
                page={page}
                totalResults={totalResults}
                handlePostDelete={handlePostDelete}
                handlePostEdit={handlePostEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Post?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
Posts.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Posts;
