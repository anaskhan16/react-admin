import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { focusPointsDelete, focusPointsGetAll } from "src/services/services";
import { AddFocusPoints } from "src/components/focusPoints/AddFocusPoints";
import { FocusPointsListToolbar } from "src/components/focusPoints/focusPoints-list-toolbar";
import { FocusPointsListResults } from "src/components/focusPoints/focusPoints-list-results";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const FocusPoints = () => {
  const [addFocusPointsStatus, setAddFocusPointsStatus] = useState(false);
  const [editFocusPointsStatus, setEditFocusPointsStatus] = useState(false);
  const [listOfFocusPoints, setListOfFocusPoints] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [focusPointIdForDeletion, setFocusPointIdForDeletion] = useState("");
  const [focusPointForEdit, setFocusPointForEdit] = useState("");

  useEffect(() => {
    if (!addFocusPointsStatus) {
      focusPointsGetAll().then((res) => {
        //GetAll Focus Points api call which returns all the Focus Points.
        if (res?.length > 0) setListOfFocusPoints(res);
      });
    }
  }, [addFocusPointsStatus, confirmOpen, editFocusPointsStatus]);

  const handleFocusPointDelete = (focusPointId) => {
    setFocusPointIdForDeletion(focusPointId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    focusPointsDelete(focusPointIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleFocusPointEdit = (focusPointId) => {
    setFocusPointForEdit(focusPointId);
    setEditFocusPointsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Focus Points| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addFocusPointsStatus || editFocusPointsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <FocusPointsListToolbar
                setAddFocusPointsStatus={setAddFocusPointsStatus}
                addFocusPointsStatus={addFocusPointsStatus}
                editFocusPointsStatus={editFocusPointsStatus}
              />
              <AddFocusPoints
                setAddFocusPointsStatus={setAddFocusPointsStatus}
                setEditFocusPointsStatus={setEditFocusPointsStatus}
                editFocusPointsStatus={editFocusPointsStatus}
                focusPointForEdit={focusPointForEdit}
                setFocusPointForEdit={setFocusPointForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <FocusPointsListToolbar
              setAddFocusPointsStatus={setAddFocusPointsStatus}
              addFocusPointsStatus={addFocusPointsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <FocusPointsListResults
                listOfFocusPoints={listOfFocusPoints}
                handleFocusPointDelete={handleFocusPointDelete}
                handleFocusPointEdit={handleFocusPointEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Focus Point?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
FocusPoints.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default FocusPoints;
