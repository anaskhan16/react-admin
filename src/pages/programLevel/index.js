import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { programLevelDelete, programLevelsGetAll } from "src/services/services";
import { AddProgramLevels } from "src/components/programLevels/AddProgramLevels";
import { ProgramLevelsListResults } from "src/components/programLevels/programLevels-list-results";
import { ProgramLevelsListToolbar } from "src/components/programLevels/programLevels-list-toolbar";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const ProgramLevels = () => {
  const [addProgramLevelsStatus, setAddProgramLevelsStatus] = useState(false);
  const [editProgramLevelsStatus, setEditProgramLevelsStatus] = useState(false);
  const [listOfProgramLevels, setListOfProgramLevels] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [programLevelIdForDeletion, setProgramLevelIdForDeletion] = useState(
    ""
  );
  const [programLevelForEdit, setProgramLevelsForEdit] = useState("");

  useEffect(() => {
    if (!addProgramLevelsStatus) {
      programLevelsGetAll().then((res) => {
        //GetAll ProgramLevels api call which returns all the ProgramLevels.
        if (res?.length > 0) setListOfProgramLevels(res);
      });
    }
  }, [addProgramLevelsStatus, confirmOpen, editProgramLevelsStatus]);

  const handleProgramLevelDelete = (programLevelId) => {
    setProgramLevelIdForDeletion(programLevelId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    programLevelDelete(programLevelIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleProgramLevelEdit = (programLevel) => {
    setProgramLevelsForEdit(programLevel);
    setEditProgramLevelsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Program Levels| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addProgramLevelsStatus || editProgramLevelsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <ProgramLevelsListToolbar
                setAddProgramLevelsStatus={setAddProgramLevelsStatus}
                addProgramLevelsStatus={addProgramLevelsStatus}
                editProgramLevelsStatus={editProgramLevelsStatus}
              />
              <AddProgramLevels
                setAddProgramLevelsStatus={setAddProgramLevelsStatus}
                setEditProgramLevelsStatus={setEditProgramLevelsStatus}
                editProgramLevelsStatus={editProgramLevelsStatus}
                programLevelForEdit={programLevelForEdit}
                setProgramLevelsForEdit={setProgramLevelsForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <ProgramLevelsListToolbar
              setAddProgramLevelsStatus={setAddProgramLevelsStatus}
              addProgramLevelsStatus={addProgramLevelsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <ProgramLevelsListResults
                listOfProgramLevels={listOfProgramLevels}
                handleProgramLevelDelete={handleProgramLevelDelete}
                handleProgramLevelEdit={handleProgramLevelEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Program Level?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
ProgramLevels.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default ProgramLevels;
