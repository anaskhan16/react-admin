import Head from "next/head";
import { Box, Container, Grid } from "@mui/material";
import { Budget } from "../components/dashboard/budget";
import { DashboardLayout } from "../components/dashboard-layout";
import { useEffect, useState } from "react";
import {
  userDelete,
  usersGetAll,
  userStatusUpdate,
} from "src/services/services";
import { UsersList } from "src/components/dashboard/list-users";
import ConfirmDialog from "src/components/common/confirm-dialog";
import { PostsCard } from "src/components/dashboard/posts";
import { TagsCard } from "src/components/dashboard/tagsCard";
import { ForumTagsCard } from "src/components/dashboard/forumTagsCard";
import { BodyPartsCard } from "src/components/dashboard/bodyPartsCard";
import { EquipmentsCard } from "src/components/dashboard/equipmentsCard";
import {
  focusPointsCard,
  FocusPointsCard,
} from "src/components/dashboard/focusPointsCard";
import { useRouter } from "next/router";

const Dashboard = () => {
  const [usersList, setUsersList] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [userIdForDeletion, setUserIdForDeletion] = useState("");
  const [statusUpdatedFlag, setStatusUpdatedFlag] = useState(0);
  const router = useRouter();

  useEffect(() => {
    usersGetAll().then((res) => {
      if (res?.length > 0) setUsersList(res);
    });
  }, [confirmOpen, statusUpdatedFlag]);

  const handleUserDelete = (userId) => {
    setUserIdForDeletion(userId);
    setConfirmOpen(true);
  };

  const handleUserEdit = (user) => {
    router.push(
      {
        pathname: "/register",
        query: user,
      },
      "/register"
    );
  };

  const handleConfirmDeletion = () => {
    userDelete(userIdForDeletion).then((res) => {
      if (res?._id) setConfirmOpen(false);
    });
  };

  const updateUserStatus = (user) => {
    userStatusUpdate(user?._id, {
      status: !user?.status,
    }).then((res) => {
      if (res?._id) {
        let temp = statusUpdatedFlag + 1;
        setStatusUpdatedFlag(temp);
      }
    });
  };

  return (
    <>
      <Head>
        <title>Dashboard | OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth={false}>
          <Grid container spacing={3}>
            <Grid item lg={12} md={12} xl={9} xs={12}>
              <UsersList
                usersList={usersList}
                handleUserDelete={handleUserDelete}
                updateUserStatus={updateUserStatus}
                handleUserEdit={handleUserEdit}
              />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/posts")}
            >
              <PostsCard />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/tags")}
            >
              <TagsCard />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/forum-tags")}
            >
              <ForumTagsCard />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/bodyParts")}
            >
              <BodyPartsCard />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/equipments")}
            >
              <EquipmentsCard />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={12}
              onClick={() => router.push("/bodyParts")}
            >
              <FocusPointsCard />
            </Grid>
          </Grid>
          <ConfirmDialog
            title="Delete User?"
            open={confirmOpen}
            setOpen={setConfirmOpen}
            onConfirm={handleConfirmDeletion}
          >
            <p>Are you sure?</p>
            <p>You will not be able to recover this item!</p>
          </ConfirmDialog>
        </Container>
      </Box>
    </>
  );
};

Dashboard.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Dashboard;
