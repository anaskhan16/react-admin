import Head from "next/head";
import { Box, Container } from "@mui/material";
import { DashboardLayout } from "../../components/dashboard-layout";
import { useEffect, useState } from "react";
import { equipmentsDelete, equipmentsGetAll } from "src/services/services";
import { AddEquipments } from "src/components/equipments/AddEquipments";
import { EquipmentsListResults } from "src/components/equipments/equipments-list-results";
import { EquipmentsListToolbar } from "src/components/equipments/equipments-list-toolbar";
import { ConfirmDialog } from "src/components/common/confirm-dialog";

const Equipments = () => {
  const [addEquipmentsStatus, setAddEquipmentsStatus] = useState(false);
  const [editEquipmentsStatus, setEditEquipmentsStatus] = useState(false);
  const [listOfEquipments, setListOfEquipments] = useState([]);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [equipmentsIdForDeletion, setEquipmentsIdForDeletion] = useState("");
  const [equipmentsForEdit, setEquipmentsForEdit] = useState("");

  useEffect(() => {
    if (!addEquipmentsStatus) {
      equipmentsGetAll().then((res) => {
        //GetAll Equipments api call which returns all the Equipments.
        if (res?.length > 0) setListOfEquipments(res);
      });
    }
  }, [addEquipmentsStatus, confirmOpen, editEquipmentsStatus]);

  const handleEquipmentsDelete = (equipmentsId) => {
    setEquipmentsIdForDeletion(equipmentsId);
    setConfirmOpen(true);
  };
  const handleConfirmDeletion = () => {
    equipmentsDelete(equipmentsIdForDeletion).then((res) => {
      setConfirmOpen(false);
    });
  };

  const handleEquipmentsEdit = (equipments) => {
    setEquipmentsForEdit(equipments);
    setEditEquipmentsStatus(true);
  };

  return (
    <>
      <Head>
        <title>Equipments| OptimiHealth</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          pt: 4,
          pb: 8,
        }}
      >
        {addEquipmentsStatus || editEquipmentsStatus ? (
          <Container maxWidth={false}>
            <Box sx={{ mt: 3 }}>
              <EquipmentsListToolbar
                setAddEquipmentsStatus={setAddEquipmentsStatus}
                addEquipmentsStatus={addEquipmentsStatus}
                editEquipmentsStatus={editEquipmentsStatus}
              />
              <AddEquipments
                setAddEquipmentsStatus={setAddEquipmentsStatus}
                setEditEquipmentsStatus={setEditEquipmentsStatus}
                editEquipmentsStatus={editEquipmentsStatus}
                equipmentsForEdit={equipmentsForEdit}
                setEquipmentsForEdit={setEquipmentsForEdit}
              />
            </Box>
          </Container>
        ) : (
          <Container maxWidth={false}>
            <EquipmentsListToolbar
              setAddEquipmentsStatus={setAddEquipmentsStatus}
              addEquipmentsStatus={addEquipmentsStatus}
            />
            <Box sx={{ mt: 3 }}>
              <EquipmentsListResults
                listOfEquipments={listOfEquipments}
                handleEquipmentsDelete={handleEquipmentsDelete}
                handleEquipmentsEdit={handleEquipmentsEdit}
              />
            </Box>
          </Container>
        )}
      </Box>
      <ConfirmDialog
        title="Delete Body Part?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirmDeletion}
      >
        <p>Are you sure?</p>
        <p>You will not be able to recover this item!</p>
      </ConfirmDialog>
    </>
  );
};
Equipments.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Equipments;
