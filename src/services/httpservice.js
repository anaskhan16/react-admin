import fetch from "isomorphic-unfetch";

class HttpService {
  // The constructor can receive the baseUrl or it can be provided via an ENV variable too.
  constructor(baseUrl) {
    //this.baseUrl = API_BASE_URL || baseUrl;
  }

  fullUrl = (url) => `${url}`;

  get = async (url, params, headers) => {
    let urlWithParams = url;
    if (params) {
      const query = Object.keys(params)
        .map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
        .join("&");
      urlWithParams = `${url}${query ? `?` : ``}${query}`;
    }

    const response = await fetch(this.fullUrl(urlWithParams), {
      method: "GET",
      headers,
    });

    return response.json();
  };

  post = async (url, body, headers) => {
    const response = await fetch(this.fullUrl(url), {
      method: "POST",
      body: JSON.stringify(body),
      headers,
    });

    return response.json();
  };

  postWithFiles = async (url, body) => {
    const response = await fetch(this.fullUrl(url), {
      method: "POST",
      body: body,
      redirect: "follow",
    });

    return response?.status > 299 ? response?.status : response.json();
  };

  postStream = async (url, body, headers) => {
    const response = await fetch(this.fullUrl(url), {
      method: "POST",
      body: JSON.stringify(body),
      headers,
    });
    return response;
  };

  delete = async (url, body, headers) => {
    const response = await fetch(this.fullUrl(url), {
      method: "DELETE",
      body: JSON.stringify(body),
      headers,
    });
    return response.json();
  };

  postFile = async (url, methodBody, headers) => {
    const response = await fetch(this.fullUrl(url), {
      method: "POST",
      headers,
      body: methodBody,
    });
    return response.json();
  };

  put = async (url, body, headers) => {
    let urlWithParams = url;
    const response = await fetch(this.fullUrl(url), {
      method: "PUT",
      body: JSON.stringify(body),
      headers,
    });
    return response.json();
  };

  putWithFiles = async (url, body) => {
    let urlWithParams = url;
    const response = await fetch(this.fullUrl(url), {
      method: "PUT",
      body: body,
      redirect: "follow",
    });
    return response.json();
  };
}

export default new HttpService();
