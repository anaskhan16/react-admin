import HttpService from "./httpservice";

const configHeaders = () => ({
  "Content-Type": "application/json",
});

//Services for Login/Auth Admin starts.

export const loginAdmin = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/admin/login`,
    body,
    configHeaders()
  );

export const signUpUser = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/professional/create`,
    body,
    configHeaders()
  );

export const usersGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/professional/getAll`,
    {},
    configHeaders()
  );
export const userDelete = (userId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/professional/delete/${userId}`,
    {},
    configHeaders()
  );

export const userStatusUpdate = (userId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/professional/check/${userId}`,
    body,
    configHeaders()
  );

export const userDataUpdate = (userId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/professional/update/${userId}`,
    body,
    configHeaders()
  );
//Services for Login/Auth Admin ends.

//Services for POSTS CRUD starts.

export const postsGetAll = (page, limit) =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/post/getAll?page=${page}&limit=${limit}`,
    {},
    configHeaders()
  );

export const postsCreate = (body) =>
  HttpService.postWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/post/create`,
    body
  );

export const postsUpdate = (postId, body) =>
  HttpService.putWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/post/update/${postId}`,
    body
  );

export const postsDelete = (tagId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/post/delete/${tagId}`,
    {},
    configHeaders()
  );
//Services for POSTS CRUD ends.

//Services for PARTNERSHIPS CRUD starts.

export const partnershipsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/partnerships/getAll`,
    {},
    configHeaders()
  );

export const partnershipsCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/partnerships/create`,
    body,
    configHeaders()
  );

export const partnershipsUpdate = (partnershipsId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/partnerships/update/${partnershipsId}`,
    body,
    configHeaders()
  );

export const partnershipsDelete = (partnershipsId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/partnerships/delete/${partnershipsId}`,
    {},
    configHeaders()
  );

//Services for PARTNERSHIPS CRUD ends.

//Services for TAGS CRUD starts.

export const tagsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/tag/getAll`,
    {},
    configHeaders()
  );

export const tagsCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/tag/create`,
    body,
    configHeaders()
  );

export const tagsUpdate = (tagId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/tag/update/${tagId}`,
    body,
    configHeaders()
  );

export const tagsDelete = (tagId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/tag/delete/${tagId}`,
    {},
    configHeaders()
  );
//Services for TAGS CRUD ends.

//Services for forumTAGS CRUD starts.

export const forumTagsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/forumTags/getAll`,
    {},
    configHeaders()
  );

export const forumTagsCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/forumTags/create`,
    body,
    configHeaders()
  );

export const forumTagsUpdate = (forumTagId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/forumTags/update/${forumTagId}`,
    body,
    configHeaders()
  );

export const forumTagsDelete = (forumTagId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/forumTags/delete/${forumTagId}`,
    {},
    configHeaders()
  );
//Services for forumTAGS CRUD ends.

//Services for exclusioncriteria CRUD starts.

export const exclusionCriteriaGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/exclusionCriteria/getAll`,
    {},
    configHeaders()
  );

export const exclusionCriteriaCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/exclusionCriteria/create`,
    body,
    configHeaders()
  );

export const exclusionCriteriaUpdate = (exclusionCriteriaId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/exclusionCriteria/update/${exclusionCriteriaId}`,
    body,
    configHeaders()
  );

export const exclusionCriteriaDelete = (exclusionCriteriaId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/exclusionCriteria/delete/${exclusionCriteriaId}`,
    {},
    configHeaders()
  );
//Services for exclusionCriteria CRUD ends.

//Services for bodyParts CRUD starts.

export const bodyPartsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/bodyParts/getAll`,
    {},
    configHeaders()
  );

export const bodyPartsCreate = (body) =>
  HttpService.postWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/bodyParts/create`,
    body
  );

export const bodyPartsUpdate = (bodyPartsId, body) =>
  HttpService.putWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/bodyParts/update/${bodyPartsId}`,
    body
  );

export const bodyPartsDelete = (bodyPartsId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/bodyParts/delete/${bodyPartsId}`,
    {},
    configHeaders()
  );
//Services for bodyParts CRUD ends.

//Services for Equipments  CRUD starts.

export const equipmentsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/equipments/getAll`,
    {},
    configHeaders()
  );

export const equipmentsCreate = (body) =>
  HttpService.postWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/equipments/create`,
    body
  );

export const equipmentsUpdate = (equipmentsId, body) =>
  HttpService.putWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/equipments/update/${equipmentsId}`,
    body
  );

export const equipmentsDelete = (equipmentsId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/equipments/delete/${equipmentsId}`,
    {},
    configHeaders()
  );
//Services for Equipments CRUD ends.

//Services for exerciseTypes CRUD starts.

export const exerciseTypeGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/exerciseType/getAll`,
    {},
    configHeaders()
  );

export const exerciseTypeCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/exerciseType/create`,
    body,
    configHeaders()
  );

export const exerciseTypeUpdate = (exerciseTypeId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/exerciseType/update/${exerciseTypeId}`,
    body,
    configHeaders()
  );

export const exerciseTypeDelete = (exerciseTypeId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/exerciseType/delete/${exerciseTypeId}`,
    {},
    configHeaders()
  );
//Services for exerciseType CRUD ends.

//Services for programLevel CRUD starts.

export const programLevelsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/programLevel/getAll`,
    {},
    configHeaders()
  );

export const programLevelsCreate = (body) =>
  HttpService.postWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/programLevel/create`,
    body,
    configHeaders()
  );

export const programLevelsUpdate = (bodyPartsId, body) =>
  HttpService.putWithFiles(
    `https://optimi-health-api.herokuapp.com/v1/programLevel/update/${bodyPartsId}`,
    body,
    configHeaders()
  );

export const programLevelDelete = (bodyPartsId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/programLevel/delete/${bodyPartsId}`,
    {},
    configHeaders()
  );
//Services for programLevel CRUD ends.

//Services for focusPointsCRUD starts.

export const focusPointsGetAll = () =>
  HttpService.get(
    `https://optimi-health-api.herokuapp.com/v1/focusPoints/getAll`,
    {},
    configHeaders()
  );

export const focusPointsCreate = (body) =>
  HttpService.post(
    `https://optimi-health-api.herokuapp.com/v1/focusPoints/create`,
    body,
    configHeaders()
  );

export const focusPointsUpdate = (focusPointId, body) =>
  HttpService.put(
    `https://optimi-health-api.herokuapp.com/v1/focusPoints/update/${focusPointId}`,
    body,
    configHeaders()
  );

export const focusPointsDelete = (focusPointId) =>
  HttpService.delete(
    `https://optimi-health-api.herokuapp.com/v1/focusPoints/delete/${focusPointId}`,
    {},
    configHeaders()
  );
//Services for focusPoints CRUD ends.
