import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { Search as SearchIcon } from "../../icons/search";

export const ForumTagsListToolbar = ({
  addForumTagsStatus,
  setAddForumTagsStatus,
  editForumTagsStatus,
}) => (
  <Box>
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        m: -1,
      }}
    >
      <Typography sx={{ m: 1 }} variant="h4">
        {!addForumTagsStatus && !editForumTagsStatus
          ? " Forum Tags"
          : editForumTagsStatus
          ? "Edit Tag"
          : "Add Forum Tags"}
      </Typography>

      {!addForumTagsStatus && !editForumTagsStatus && (
        <>
          <Box sx={{ maxWidth: 500 }}>
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon color="action" fontSize="small">
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                ),
              }}
              placeholder="Search"
              variant="outlined"
            />
          </Box>

          <Box sx={{ m: 1 }}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => setAddForumTagsStatus(true)}
            >
              Add Forum Tags
            </Button>
          </Box>
        </>
      )}
    </Box>
  </Box>
);
