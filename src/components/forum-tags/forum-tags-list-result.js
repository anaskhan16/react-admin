import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const ForumTagsListResults = ({
  listOfForumTags,
  handleForumTagsDelete,
  handleForumTagsEdit,
  ...rest
}) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfForumTags?.length > 0 &&
                listOfForumTags.slice(0, limit).map((forumTags, id) => (
                  <TableRow hover key={forumTags?._id}>
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>{forumTags?.title}</TableCell>
                    <TableCell>
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() => handleForumTagsDelete(forumTags?._id)}
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handleForumTagsEdit(forumTags)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={listOfForumTags.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

ForumTagsListResults.propTypes = {};
