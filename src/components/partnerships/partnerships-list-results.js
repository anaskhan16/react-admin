import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const PartnershipsListResults = ({
  listOfPartnerships,
  handlePartnershipsDelete,
  handlePartnershipsEdit,
  ...rest
}) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Website URL</TableCell>
                <TableCell> Affiliation Link</TableCell>
                <TableCell>Discount</TableCell>
                <TableCell>Logo (CDN FILE NAME)</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfPartnerships?.length > 0 &&
                listOfPartnerships.slice(0, limit).map((partnerships, id) => (
                  <TableRow hover key={partnerships?._id}>
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>{partnerships?.title}</TableCell>
                    <TableCell>{partnerships?.description}</TableCell>
                    <TableCell>{partnerships?.websiteUrl}</TableCell>
                    <TableCell>{partnerships?.affiliationLink}</TableCell>
                    <TableCell>{partnerships?.discount}</TableCell>
                    <TableCell>{partnerships?.logo}</TableCell>
                    <TableCell>
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() =>
                          handlePartnershipsDelete(partnerships?._id)
                        }
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handlePartnershipsEdit(partnerships)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={listOfPartnerships.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

PartnershipsListResults.propTypes = {};
