import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";
import { partnershipsCreate, partnershipsUpdate } from "src/services/services";

export const AddPartners = ({
  setAddParnershipsStatus,
  editPartnershipsStatus,
  partnershipsForEdit,
  setEditPartnershipsStatus,
  setPartnershipsForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
    description: "",
    websiteUrl: "",
    affiliationLink: "",
    discount: "",
    logo: "",
  });

  useEffect(() => {
    if (editPartnershipsStatus) {
      setValues({
        ...values,
        title: partnershipsForEdit?.title,
        description: partnershipsForEdit?.description,
        websiteUrl: partnershipsForEdit?.websiteUrl,
        affiliationLink: partnershipsForEdit?.affiliationLink,
        discount: partnershipsForEdit?.discount,
        logo: partnershipsForEdit?.logo,
      });
    }
  }, []);

  const handleChange = (event) => {
    event.preventDefault();
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    if (values?.title?.length > 0) {
      if (editPartnershipsStatus) {
        partnershipsUpdate(partnershipsForEdit?._id, values).then((res) => {
          if (res?.responseStatus?.success) {
            setPartnershipsForEdit("");
            setEditPartnershipsStatus(false);
          }
        });
      } else {
        partnershipsCreate(values).then((res) => {
          if (res?.responseStatus?.success) {
            setAddParnershipsStatus(false);
          }
        });
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form autoComplete="off" onSubmit={(e) => handleSubmitInformation(e)}>
            <Card>
              <CardHeader
                subheader="New Partner can be added"
                title="New Parner"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Description"
                      name="description"
                      onChange={handleChange}
                      required
                      value={values.description}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Website URL"
                      name="websiteUrl"
                      onChange={handleChange}
                      required
                      value={values.websiteUrl}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Affiliation Link"
                      name="affiliationLink"
                      onChange={handleChange}
                      required
                      value={values.affiliationLink}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Discount"
                      name="discount"
                      onChange={handleChange}
                      required
                      value={values.discount}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Logo (CDN File Name)"
                      name="logo"
                      onChange={handleChange}
                      required
                      value={values.logo}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Upload
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddParnershipsStatus(false);
                    setEditPartnershipsStatus(false);
                    setPartnershipsForEdit("");
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddPartners.propTypes = {
  setAddPartnershipsStatus: PropTypes.func.isRequired,
};
