import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { Search as SearchIcon } from "../../icons/search";

export const PostsListToolbar = ({ addPostsStatus, setAddPostsStatus }) => (
  <Box>
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        m: -1,
      }}
    >
      <Typography sx={{ m: 1 }} variant="h4">
        {!addPostsStatus ? "Posts" : "Add Posts"}
      </Typography>

      {!addPostsStatus && <><Box sx={{ maxWidth: 500 }}>
        <TextField
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon color="action" fontSize="small">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            ),
          }}
          placeholder="Search"
          variant="outlined"
        />
      </Box>

      <Box sx={{ m: 1 }}>
        <Button
          color="primary"
          variant="contained"
          onClick={() => setAddPostsStatus(true)}
        >
          Add Posts
        </Button>
      </Box></>}
    </Box>
  </Box>
);
