import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const PostsListResults = ({
  customers,
  listOfPosts,
  listOfTags,
  limit,
  setLimit,
  page,
  setPage,
  totalResults,
  handlePostDelete,
  handlePostEdit,
  ...rest
}) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);

  const returnTagString = (id) => {
    const tagFound = listOfTags.filter((obj) => obj?._id === id);
    if (tagFound?.length > 0) {
      return tagFound?.[0]?.title;
    }
    return "-";
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    setPage(0);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Image</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Tag</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Featured</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfPosts?.length > 0 &&
                listOfPosts.slice(0, limit).map((post, id) => (
                  <TableRow
                    hover
                    key={post?._id}
                    //   selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                  >
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>
                      <img src={post?.image} width="90"></img>
                    </TableCell>
                    <TableCell>{post?.title}</TableCell>
                    <TableCell>{returnTagString(post?.tag)}</TableCell>
                    <TableCell>
                      {format(new Date(post?.updatedAt), "dd MMMM yyyy")}
                    </TableCell>
                    <TableCell>{post?.featured ? "Yes" : "No"}</TableCell>
                    <TableCell>
                      {post?.status === "Publish" ? "Yes" : "No"}
                    </TableCell>
                    <TableCell>
                      {" "}
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() => handlePostDelete(post?._id)}
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handlePostEdit(post)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={totalResults}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

PostsListResults.propTypes = {
  customers: PropTypes.array.isRequired,
};
