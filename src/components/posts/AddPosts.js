import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";

import { format } from "date-fns";

import {
  Checkbox,
  Typography,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import { postsCreate, postsUpdate } from "src/services/services";
import FormData from "form-data";
export const AddPosts = ({
  setAddPostsStatus,
  listOfTags,
  editPostsStatus,
  postForEdit,
  setEditPostsStatus,
  setPostForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
    description: "",
    tag: listOfTags?.[0]?._id,
    featured: "",
    status: "",
    image: null,
  });
  useEffect(() => {
    if (editPostsStatus) {
      setValues({
        ...values,
        title: postForEdit?.title,
        description: postForEdit?.description,
        tag: postForEdit?.tag,
        featured: postForEdit?.featured,
        status: postForEdit?.status,
        image: postForEdit?.image,
      });
    }
  }, []);

  const handleChange = (event) => {
    if (event.target.name === "image") {
      setValues({
        ...values,
        [event.target.name]: event.target.files[0],
      });
    } else
      setValues({
        ...values,
        [event.target.name]: event.target.value,
      });
  };

  const handleResetFormFields = () => {
    setValues({
      title: "",
      description: "",
      tag: listOfTags?.[0]?._id,
      featured: "",
      status: "",
      image: null,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    const form = new FormData();
    form.append("title", values?.title);
    form.append("description", values?.description);
    form.append("tag", values?.tag);
    form.append("featured", values?.featured);
    form.append("status", values?.status);
    form.append("image", values?.image);
    if (
      values?.title?.length > 0 &&
      values?.description?.length > 0 &&
      values?.tag?.length > 0 &&
      values?.status?.length > 0 &&
      values?.image
    ) {
      var requestOptions = {
        method: "POST",
        body: form,
        redirect: "follow",
      };

      if (editPostsStatus) {
        postsUpdate(postForEdit?._id, form).then((res) => {
          if (res?.responseStatus?.success) {
            setPostForEdit({});
            setEditPostsStatus(false);
          }
        });
      } else {
        postsCreate(form)
          .then((res) => {
            if (res?.responseStatus?.success) {
              setAddPostsStatus(false);
            }
          })
          .catch((error) => console.log("error", error));
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form
            autoComplete="off"
            noValidate
            onSubmit={(e) => handleSubmitInformation(e)}
          >
            <Card>
              <CardHeader
                subheader="New information can be added"
                title="New Post"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Description"
                      name="description"
                      onChange={handleChange}
                      required
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                  {/* <Grid item md={12} xs={12} mb={5}>
                    <Box sx={{ border: 1 }}>
                      <MUIRichTextEditor
                        label="Type something here..."
                        onSave={save}
                        inlineToolbar={true}
                      />
                    </Box>
                  </Grid> */}
                  <Grid item md={6} xs={12}>
                    <TextField
                      fullWidth
                      label="Tag"
                      name="tag"
                      onChange={handleChange}
                      required
                      select
                      SelectProps={{ native: true }}
                      defaultValue={
                        listOfTags?.length ? listOfTags?.[0]?._id : values?.tag
                      }
                      value={values?.tag}
                      variant="outlined"
                    >
                      {listOfTags?.length > 0 &&
                        listOfTags.map((option) => (
                          <option key={option._id} value={option._id}>
                            {option.title}
                          </option>
                        ))}
                    </TextField>
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <FormControl>
                      <FormLabel id="demo-row-radio-buttons-group-label">
                        Featured
                      </FormLabel>
                      <RadioGroup
                        row
                        name="featured"
                        value={values.featured}
                        onChange={handleChange}
                      >
                        <FormControlLabel
                          value={true}
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value={false}
                          control={<Radio />}
                          label="No"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <FormControl>
                      <FormLabel id="demo-row-radio-buttons-group-label">
                        Status
                      </FormLabel>
                      <RadioGroup
                        row
                        name="status"
                        value={values.status}
                        onChange={handleChange}
                      >
                        <FormControlLabel
                          value={"Publish"}
                          control={<Radio />}
                          label="Publish"
                        />
                        <FormControlLabel
                          value={"Draft"}
                          control={<Radio />}
                          label="Draft"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <input
                      // className={classes.input}
                      name="image"
                      onChange={handleChange}
                      type="file"
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Save details
                </Button>
                <Button
                  color="primary"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => handleResetFormFields()}
                >
                  Reset
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddPostsStatus(false);
                    setEditPostsStatus(false);
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddPosts.propTypes = {
  setAddPostsStatus: PropTypes.func.isRequired,
};
