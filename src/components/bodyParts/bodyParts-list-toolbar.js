import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { Search as SearchIcon } from "../../icons/search";

export const BodyPartsListToolbar = ({
  addBodyPartsStatus,
  setAddBodyPartsStatus,
  editBodyPartsStatus,
}) => (
  <Box>
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        m: -1,
      }}
    >
      <Typography sx={{ m: 1 }} variant="h4">
        {!addBodyPartsStatus && !editBodyPartsStatus
          ? "Body Parts"
          : editBodyPartsStatus
          ? "Edit Body Part"
          : "Add Body Part"}
      </Typography>

      {!addBodyPartsStatus && !editBodyPartsStatus && (
        <>
          <Box sx={{ maxWidth: 500 }}>
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon color="action" fontSize="small">
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                ),
              }}
              placeholder="Search"
              variant="outlined"
            />
          </Box>

          <Box sx={{ m: 1 }}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => setAddBodyPartsStatus(true)}
            >
              Add Body Part
            </Button>
          </Box>
        </>
      )}
    </Box>
  </Box>
);
