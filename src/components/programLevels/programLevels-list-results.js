import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const ProgramLevelsListResults = ({
  listOfProgramLevels,
  handleProgramLevelDelete,
  handleProgramLevelEdit,
  totalResults,
  ...rest
}) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    setPage(0);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Image</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfProgramLevels?.length > 0 &&
                listOfProgramLevels.slice(0, limit).map((programLevels, id) => (
                  <TableRow hover key={programLevels?._id}>
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>{programLevels?.title}</TableCell>
                    <TableCell>
                      <img src={programLevels?.image} width="90"></img>
                    </TableCell>
                    <TableCell>
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() =>
                          handleProgramLevelDelete(programLevels?._id)
                        }
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handleProgramLevelEdit(programLevels)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={listOfProgramLevels.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

ProgramLevelsListResults.propTypes = {};
