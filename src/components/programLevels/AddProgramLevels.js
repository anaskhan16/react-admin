import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";
import {
  programLevelsCreate,
  programLevelsUpdate,
} from "src/services/services";
import FormData from "form-data";

export const AddProgramLevels = ({
  setAddProgramLevelsStatus,
  editProgramLevelsStatus,
  programLevelForEdit,
  setEditProgramLevelsStatus,
  setProgramLevelsForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
    image: null,
  });

  useEffect(() => {
    if (editProgramLevelsStatus) {
      setValues({
        ...values,
        title: programLevelForEdit?.title,
        image: programLevelForEdit?.image,
      });
    }
  }, []);

  const handleChange = (event) => {
    if (event.target.name === "image") {
      setValues({
        ...values,
        [event.target.name]: event.target.files[0],
      });
    } else
      setValues({
        ...values,
        [event.target.name]: event.target.value,
      });
  };

  const handleResetFormFields = () => {
    setValues({
      title: "",
      image: null,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    const form = new FormData();
    form.append("title", values?.title);
    form.append("image", values?.image);
    if (values?.title?.length > 0 && values?.image) {
      if (editProgramLevelsStatus) {
        programLevelsUpdate(programLevelForEdit?._id, form).then((res) => {
          if (res?.responseStatus?.success) {
            setProgramLevelsForEdit({});
            setEditProgramLevelsStatus(false);
          }
        });
      } else {
        programLevelsCreate(form)
          .then((res) => {
            if (res?.responseStatus?.success) {
              setAddProgramLevelsStatus(false);
            }
          })
          .catch((error) => console.log("error", error));
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form
            autoComplete="off"
            noValidate
            onSubmit={(e) => handleSubmitInformation(e)}
          >
            <Card>
              <CardHeader
                subheader="New  Program Level can be added"
                title="New Program Level"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <input
                      // className={classes.input}
                      name="image"
                      onChange={handleChange}
                      type="file"
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Save
                </Button>
                <Button
                  color="primary"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => handleResetFormFields()}
                >
                  Reset
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddProgramLevelsStatus(false);
                    setEditProgramLevelsStatus(false);
                    setProgramLevelsForEdit("");
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddProgramLevels.propTypes = {
  setAddProgramLevelsStatus: PropTypes.func.isRequired,
};
