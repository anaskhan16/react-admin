import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const ExclusionCriteriaListResults = ({
  listOfExclusionCriteria,
  handleExclusionCriteriaDelete,
  handleExclusionCriteriaEdit,
  ...rest
}) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfExclusionCriteria?.length > 0 &&
                listOfExclusionCriteria.slice(0, limit).map((exclusion, id) => (
                  <TableRow hover key={exclusion?._id}>
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>{exclusion?.title}</TableCell>
                    <TableCell>
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() =>
                          handleExclusionCriteriaDelete(exclusion?._id)
                        }
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handleExclusionCriteriaEdit(exclusion)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={listOfExclusionCriteria.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

ExclusionCriteriaListResults.propTypes = {};
