import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";
import {
  exclusionCriteriaCreate,
  exclusionCriteriaUpdate,
} from "src/services/services";

export const AddExclusionCriteria = ({
  setAddExclusionCriteriaStatus,
  editExclusionCriteriaStatus,
  exclusionCriteriaForEdit,
  setEditExclusionCriteriaStatus,
  setExclusionCriteriaForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
  });

  useEffect(() => {
    if (editExclusionCriteriaStatus) {
      setValues({
        ...values,
        title: exclusionCriteriaForEdit?.title,
      });
    }
  }, []);

  const handleChange = (event) => {
    event.preventDefault();
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    if (values?.title?.length > 0) {
      if (editExclusionCriteriaStatus) {
        exclusionCriteriaUpdate(exclusionCriteriaForEdit?._id, values).then(
          (res) => {
            if (res?.responseStatus?.success) {
              setExclusionCriteriaForEdit("");
              setEditExclusionCriteriaStatus(false);
            }
          }
        );
      } else {
        exclusionCriteriaCreate(values).then((res) => {
          if (res?.responseStatus?.success) {
            setAddExclusionCriteriaStatus(false);
          }
        });
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form autoComplete="off" onSubmit={(e) => handleSubmitInformation(e)}>
            <Card>
              <CardHeader
                subheader="New exclusion can be added"
                title="New Exclusion"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Upload
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddExclusionCriteriaStatus(false);
                    setEditExclusionCriteriaStatus(false);
                    setExclusionCriteriaForEdit("");
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddExclusionCriteria.propTypes = {
  setAddExclusionCriteriaStatus: PropTypes.func.isRequired,
};
