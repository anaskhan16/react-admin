import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { format } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { getInitials } from "../../utils/get-initials";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

export const FocusPointsListResults = ({
  listOfFocusPoints,
  handleFocusPointDelete,
  handleFocusPointEdit,
  ...rest
}) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listOfFocusPoints?.length > 0 &&
                listOfFocusPoints.slice(0, limit).map((focusPoints, id) => (
                  <TableRow hover key={focusPoints?._id}>
                    <TableCell>{id + 1}</TableCell>
                    <TableCell>{focusPoints?.title}</TableCell>
                    <TableCell>
                      <DeleteIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() => handleFocusPointDelete(focusPoints?._id)}
                      />
                      <EditIcon
                        sx={{ cursor: "pointer", ml: 2 }}
                        onClick={() => handleFocusPointEdit(focusPoints)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={listOfFocusPoints.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

FocusPointsListResults.propTypes = {};
