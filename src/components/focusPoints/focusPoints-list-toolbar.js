import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { Search as SearchIcon } from "../../icons/search";

export const FocusPointsListToolbar = ({
  addFocusPointsStatus,
  setAddFocusPointsStatus,
  editFocusPointsStatus,
}) => (
  <Box>
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        m: -1,
      }}
    >
      <Typography sx={{ m: 1 }} variant="h4">
        {!addFocusPointsStatus && !editFocusPointsStatus
          ? "Focus Points"
          : editFocusPointsStatus
          ? "Edit FocusPoint"
          : "Add FocusPoint"}
      </Typography>

      {!addFocusPointsStatus && !editFocusPointsStatus && (
        <>
          <Box sx={{ maxWidth: 500 }}>
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon color="action" fontSize="small">
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                ),
              }}
              placeholder="Search"
              variant="outlined"
            />
          </Box>

          <Box sx={{ m: 1 }}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => setAddFocusPointsStatus(true)}
            >
              Add Focus Point
            </Button>
          </Box>
        </>
      )}
    </Box>
  </Box>
);
