import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";
import { focusPointsCreate, focusPointsUpdate } from "src/services/services";

export const AddFocusPoints = ({
  setAddFocusPointsStatus,
  editFocusPointsStatus,
  focusPointForEdit,
  setEditFocusPointsStatus,
  setFocusPointForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
  });

  useEffect(() => {
    if (editFocusPointsStatus) {
      setValues({
        ...values,
        title: focusPointForEdit?.title,
      });
    }
  }, []);

  const handleChange = (event) => {
    event.preventDefault();
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    if (values?.title?.length > 0) {
      if (editFocusPointsStatus) {
        focusPointsUpdate(focusPointForEdit?._id, values).then((res) => {
          if (res?.responseStatus?.success) {
            setFocusPointForEdit("");
            setEditFocusPointsStatus(false);
          }
        });
      } else {
        focusPointsCreate(values).then((res) => {
          if (res?.responseStatus?.success) {
            setAddFocusPointsStatus(false);
          }
        });
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form autoComplete="off" onSubmit={(e) => handleSubmitInformation(e)}>
            <Card>
              <CardHeader
                subheader="New Focus Point can be added"
                title="New Focus Point"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Upload
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddFocusPointsStatus(false);
                    setEditFocusPointsStatus(false);
                    setFocusPointForEdit("");
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddFocusPoints.propTypes = {
  setAddFocusPointsStatus: PropTypes.func.isRequired,
};
