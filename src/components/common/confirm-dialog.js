import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
} from "@mui/material";

export const ConfirmDialog = (props) => {
  const { title, children, open, setOpen, onConfirm } = props;
  return (
    <Dialog
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="confirm-dialog"
    >
      <DialogTitle id="confirm-dialog">{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={() => setOpen(false)} color="info">
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={() => {
            onConfirm();
          }}
          color="error"
        >
          Yes,Delete it!
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default ConfirmDialog;
