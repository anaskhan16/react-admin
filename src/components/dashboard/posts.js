import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import ArticleIcon from "@mui/icons-material/Article";
import { useEffect, useState } from "react";
import { postsGetAll } from "src/services/services";

export const PostsCard = () => {
  const [countPosts, setCountPosts] = useState(0);

  useEffect(() => {
    postsGetAll(1, 10).then((res) => {
      if (res?.results?.length > 0) {
        setCountPosts(res?.totalResults);
      }
    });
  }, []);

  return (
    <Card sx={{ height: "100%" }}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: "space-between" }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="overline">
              POSTS
            </Typography>
            <Typography color="textPrimary" variant="h4">
              {countPosts}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: "success.main",
                height: 56,
                width: 56,
              }}
            >
              <ArticleIcon />
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
