import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";

import HomeRepairServiceIcon from "@mui/icons-material/HomeRepairService";
import { useEffect, useState } from "react";
import { equipmentsGetAll } from "src/services/services";

export const EquipmentsCard = () => {
  const [countEquipments, setCountEquipments] = useState(0);

  useEffect(() => {
    equipmentsGetAll().then((res) => {
      //GetAll  Equipments api call which returns all the  Equipments.
      if (res?.length > 0) setCountEquipments(res?.length);
    });
  }, []);

  return (
    <Card sx={{ height: "100%" }}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: "space-between" }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="overline">
              EQUIPMENTS{" "}
            </Typography>
            <Typography color="textPrimary" variant="h4">
              {countEquipments}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: "info.main",
                height: 56,
                width: 56,
              }}
            >
              <HomeRepairServiceIcon />
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
