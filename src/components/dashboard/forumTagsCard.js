import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import TagIcon from "@mui/icons-material/Tag";
import { useEffect, useState } from "react";
import { forumTagsGetAll } from "src/services/services";

export const ForumTagsCard = () => {
  const [countTags, setCountTags] = useState(0);

  useEffect(() => {
    forumTagsGetAll().then((res) => {
      //GetAll tags api call which returns all the tags.
      if (res?.length > 0) setCountTags(res?.length);
    });
  }, []);

  return (
    <Card sx={{ height: "100%" }}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: "space-between" }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="overline">
              FORUM TAGS{" "}
            </Typography>
            <Typography color="textPrimary" variant="h4">
              {countTags}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: "info.main",
                height: 56,
                width: 56,
              }}
            >
              <TagIcon />
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
