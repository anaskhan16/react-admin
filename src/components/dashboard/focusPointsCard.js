import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import CenterFocusStrongIcon from "@mui/icons-material/CenterFocusStrong";
import { useEffect, useState } from "react";
import { focusPointsGetAll } from "src/services/services";

export const FocusPointsCard = () => {
  const [countFocusPoints, setCountFocusPoints] = useState(0);

  useEffect(() => {
    focusPointsGetAll().then((res) => {
      //GetAll FocusPoints api call which returns all the FocusPoints.
      if (res?.length > 0) setCountFocusPoints(res?.length);
    });
  }, []);

  return (
    <Card sx={{ height: "100%" }}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: "space-between" }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="overline">
              FOCUSPOINTS{" "}
            </Typography>
            <Typography color="textPrimary" variant="h4">
              {countFocusPoints}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: "info.main",
                height: 56,
                width: 56,
              }}
            >
              <CenterFocusStrongIcon />
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
