import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import DirectionsRunOutlinedIcon from "@mui/icons-material/DirectionsRunOutlined";
import { useEffect, useState } from "react";
import { bodyPartsGetAll } from "src/services/services";

export const BodyPartsCard = () => {
  const [countBodyParts, setCountBodyParts] = useState(0);

  useEffect(() => {
    bodyPartsGetAll().then((res) => {
      //GetAll BodyParts api call which returns all the BodyParts.
      if (res?.length > 0) setCountBodyParts(res?.length);
    });
  }, []);

  return (
    <Card sx={{ height: "100%" }}>
      <CardContent>
        <Grid container spacing={3} sx={{ justifyContent: "space-between" }}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="overline">
              BODYPARTS{" "}
            </Typography>
            <Typography color="textPrimary" variant="h4">
              {countBodyParts}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: "info.main",
                height: 56,
                width: 56,
              }}
            >
              <DirectionsRunOutlinedIcon />
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
