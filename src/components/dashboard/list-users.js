import { format } from "date-fns";
import { v4 as uuid } from "uuid";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Box,
  Button,
  Card,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Tooltip,
} from "@mui/material";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import { SeverityPill } from "../severity-pill";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { useEffect, useState } from "react";

export const UsersList = ({
  usersList,
  handleUserDelete,
  updateUserStatus,
  handleUserEdit,
}) => {
  const [viewAll, setViewAll] = useState(false);

  return (
    <>
      <Card>
        <CardHeader title="List of all Users" />
        <PerfectScrollbar>
          <Box sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>User </TableCell>
                  {/* <TableCell sortDirection="desc">
                      <Tooltip enterDelay={300} title="Sort">
                        <TableSortLabel active direction="desc">
                          Updated
                        </TableSortLabel>
                      </Tooltip>
                    </TableCell> */}
                  <TableCell>Updated</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>City</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {usersList?.length > 0 &&
                  (viewAll ? usersList : usersList.slice(0, 5))?.map((user) => (
                    <TableRow hover key={user._id}>
                      <TableCell>{user._id}</TableCell>
                      <TableCell>
                        {user.firstName + " " + user.lastName}
                      </TableCell>
                      <TableCell>
                        {format(new Date(user.updatedAt), "dd/MM/yyyy")}
                      </TableCell>
                      <TableCell>{user.email}</TableCell>
                      <TableCell>{user.city}</TableCell>

                      <TableCell>
                        <SeverityPill
                          color={user.status ? "success" : "error"}
                          onClick={() => updateUserStatus(user)}
                          sx={{ cursor: "pointer" }}
                        >
                          {user.status ? "Active" : "InActive"}
                        </SeverityPill>
                      </TableCell>
                      <TableCell>
                        <DeleteIcon
                          sx={{ cursor: "pointer" }}
                          onClick={() => handleUserDelete(user?._id)}
                        />
                        <EditIcon
                          sx={{ cursor: "pointer", ml: 2 }}
                          onClick={() => handleUserEdit(user)}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
        {usersList?.length > 5 && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              p: 2,
            }}
          >
            <Button
              color="primary"
              endIcon={<ArrowRightIcon fontSize="small" />}
              size="small"
              variant="text"
              onClick={() => setViewAll(!viewAll)}
            >
              {!viewAll ? "View all" : "View Less"}
            </Button>
          </Box>
        )}
      </Card>
    </>
  );
};
