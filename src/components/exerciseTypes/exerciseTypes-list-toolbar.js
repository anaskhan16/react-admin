import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { Search as SearchIcon } from "../../icons/search";

export const ExerciseTypesListToolbar = ({
  addExerciseTypesStatus,
  setAddExerciseTypesStatus,
  editExerciseTypesStatus,
}) => (
  <Box>
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        m: -1,
      }}
    >
      <Typography sx={{ m: 1 }} variant="h4">
        {!addExerciseTypesStatus && !editExerciseTypesStatus
          ? "Exercise Types"
          : editExerciseTypesStatus
          ? "Edit Exercise Type"
          : "Add Exercise Type"}
      </Typography>

      {!addExerciseTypesStatus && !editExerciseTypesStatus && (
        <>
          <Box sx={{ maxWidth: 500 }}>
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon color="action" fontSize="small">
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                ),
              }}
              placeholder="Search"
              variant="outlined"
            />
          </Box>

          <Box sx={{ m: 1 }}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => setAddExerciseTypesStatus(true)}
            >
              Add Exercise Type
            </Button>
          </Box>
        </>
      )}
    </Box>
  </Box>
);
