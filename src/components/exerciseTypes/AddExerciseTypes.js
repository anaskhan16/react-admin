import { useState, useEffect } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";
import { exerciseTypeCreate, exerciseTypeUpdate } from "src/services/services";

export const AddExerciseTypes = ({
  setAddExerciseTypesStatus,
  editExerciseTypesStatus,
  exerciseTypeForEdit,
  setEditExerciseTypesStatus,
  setExerciseTypeForEdit,
  ...rest
}) => {
  const [values, setValues] = useState({
    title: "",
  });

  useEffect(() => {
    if (editExerciseTypesStatus) {
      setValues({
        ...values,
        title: exerciseTypeForEdit?.title,
      });
    }
  }, []);

  const handleChange = (event) => {
    event.preventDefault();
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmitInformation = (event) => {
    event.preventDefault();
    if (values?.title?.length > 0) {
      if (editExerciseTypesStatus) {
        exerciseTypeUpdate(exerciseTypeForEdit?._id, values).then((res) => {
          if (res?.responseStatus?.success) {
            setExerciseTypeForEdit("");
            setEditExerciseTypesStatus(false);
          }
        });
      } else {
        exerciseTypeCreate(values).then((res) => {
          if (res?.responseStatus?.success) {
            setAddExerciseTypesStatus(false);
          }
        });
      }
    }
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <form autoComplete="off" onSubmit={(e) => handleSubmitInformation(e)}>
            <Card>
              <CardHeader
                subheader="New Exercise Type can be added"
                title="New Exercise Type"
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Title"
                      name="title"
                      onChange={handleChange}
                      required
                      value={values.title}
                      variant="outlined"
                      onKeyPress={(e) => {
                        if (e.key === "Enter") {
                          handleSubmitInformation(e);
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  p: 2,
                  pr: 4,
                }}
              >
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={(e) => handleSubmitInformation(e)}
                >
                  Upload
                </Button>
                <Button
                  color="error"
                  sx={{ ml: 2 }}
                  variant="contained"
                  onClick={() => {
                    setAddExerciseTypesStatus(false);
                    setEditExerciseTypesStatus(false);
                    setExerciseTypeForEdit("");
                  }}
                >
                  Cancel
                </Button>
              </Box>
            </Card>
          </form>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};

AddExerciseTypes.propTypes = {
  setAddExerciseTypesStatus: PropTypes.func.isRequired,
};
